# Projet bot

### site1.php

* Récupère et intègre les données html extraites du premier site

### site2.php

* Récupère et intègre les données html extraites du second site

### bot.html

* Formulaire html qui renvoie les fichiers php

### Choix des sites: 

1. http://www.cartesfrance.fr/carte-france-ville/photos_73065_Chambery.html

2. http://www.cartesfrance.fr/Chambery-73000/carte-Chambery.html

### Création du fichier db.sql 

1. Ajout de 3 tables liées par l'id à la table images
2. Ajout colonnes id, src, alt à la table images
3. Ajout colonnes id, infos aux deux autres tables

### Utilisation de composer

1. Installation 
2. Installation packages Guzzle et DomCrawler
3. Appel dans les fichiers php site1.php et site2.php


