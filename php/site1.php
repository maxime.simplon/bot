<?php

require 'vendor/autoload.php';
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

//récupération 1er site 
$client=new GuzzleHttp\Client();
$res=$client->request('GET', 'http://www.cartesfrance.fr/carte-france-ville/photos_73065_Chambery.html');
$domBody=$res->getBody();

//Connexion à la bdd via PDO
try{
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/database.db');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT

$crawler=new Crawler((string) $domBody);
$src=$crawler->filterXPath('//img [@class="miniature_photo"]')->extract(array('src'));
$alt=$crawler->filterXPath('//img [@class="miniature_photo"]')->extract(array('alt'));
$descriptif=$crawler->filterXPath('//div [@class="photo-description-inner"]')->extract(array('_text'));

for ($i = 0; $i < count($src); ++$i) {
    $stmt = $pdo->prepare("INSERT INTO images (src, alt) VALUES (:src, :alt)");
    $stmt2=$pdo->prepare("INSERT INTO infos (descriptif, id_images) VALUES (:descriptif, :id_images)");
    //$result= $stmt->execute(array(':src' => $src[$i], ':alt' => $alt[$i])); $id = $pdo->lastInsertId();   
    //$result2= $stmt2->execute(array(':descriptif' => $descriptif[$i], ':id_images' => $id));
 };

 //output in bot.html 
print "<h1>Table images</h1>";
 print "<table border=1>";
 print "<tr><td>id</td><td>src</td><td>alt</td></tr>";
 $result = $pdo->query('SELECT * FROM images');
 foreach($result as $row) {
   print "<tr><td>".$row['id']."</td>";
   print '<td width=33%><img src="' .$row['src'] .'" /></td>';
   print "<td width=33%>".$row['alt']."</td></tr>";
 }
 print "</table>";

 print "<h1>Table infos</h1>";
 print "<table border=1>";
 print "<tr><td>id</td><td>descriptif</td><td>id_images</td></tr>";
 $result2 = $pdo->query('SELECT * FROM infos');
 foreach($result2 as $row) {
    print "<tr><td>".$row['id']."</td>";
 print "<td width=33%>".$row['descriptif']."</td>";
 print "<td width=33%>".$row['id_images']."</td></tr>";
}
print "</table>";

}
catch(Exception $e) {
    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
    die();
};
    
?>