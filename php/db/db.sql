
CREATE TABLE images (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	src TEXT,
        alt TEXT
);
CREATE TABLE infos (
	id INTEGER  PRIMARY KEY AUTOINCREMENT,
	descriptif TEXT,
	id_images INT,
	FOREIGN KEY(id_images) REFERENCES images(id)
);


