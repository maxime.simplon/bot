<?php 

require 'vendor/autoload.php';
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

//récupération 2ème site
$client=new GuzzleHttp\Client();
$res=$client->request('GET', 'http://www.cartesfrance.fr/carte-france-ville/hotel_73065_Chambery.html');
$domBody=$res->getBody();

//Connexion à la bdd via PDO
try{
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/database.db');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT

//Récupération et intégration dans la bdd des données html extraites via DomCrawler
    $crawler=new Crawler((string) $domBody);
    $src = $crawler->filterXPath('//img [@style="margin-top:7px;margin-bottom:5px;border: 1px solid #000000;vertical-align:middle;"]')->extract(array('src'));
    $alt = $crawler->filterXPath('//img [@style="margin-top:7px;margin-bottom:5px;border: 1px solid #000000;vertical-align:middle;"]')->extract(array('alt'));
$descriptif = $crawler->filterXPath('//a [@style="align:left;color:#0060a0;font-size:15px;font-weight:bold;"]')->extract(array("_text"));

for ($i = 0; $i < count($src); ++$i) {
    $stmt = $pdo->prepare("INSERT INTO images (src, alt) VALUES (:src, :alt)");
    $stmt2=$pdo->prepare("INSERT INTO infos (descriptif, id_images) VALUES (:descriptif, :id_images)");
    //$result= $stmt->execute(array(':src' => $src[$i], ':alt' => $alt[$i])); $id = $pdo->lastInsertId();   
    //$result2= $stmt2->execute(array(':descriptif' => $descriptif[$i], ':id_images' => $id));
    echo'<img src="' . $src[$i] .'" />';
    echo $alt[$i] . '<br />';
    echo $descriptif[$i] . '<br />';

    };
}
catch(Exception $e) {
    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
    die();
};

?>